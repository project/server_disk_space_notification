<?php
/**
 * @file
 * @param $form, $form_state
 * @return $form
 */
function _server_disk_space_notification_settings_form() {
  $form = array();
  $form['server_space'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('server_space', 0),
    '#title' => t('Server Disk Space notification'),
    '#description' => t('Check if you want to get email notification for server disk space and memory.'),
   );
  $form['server_space_email_to'] = array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => variable_get('server_space_email_to', ''),
    '#title' => t('Email to'),
    '#description' => t('Enter multiple email address to get notification email. Please enter email address one by one without adding comma at the end.'),
  );
  $form['server_space_email_from_address'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('server_space_email_from_address', ''),
    '#title' => t('E-mail from address'),
    '#description' => t('The e-mail address that all e-mails will be from.'),
  );

  $form['server_space_email_from_name'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('server_space_email_from_name', ''),
    '#title' => t('E-mail from name'),
    '#description' => t('The name that all e-mails will be from. If left blank will use the site name of:') . ' ' . variable_get('site_name', 'Drupal powered site'),
  );

  $form['server_space_cron'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('server_space_cron', 0),
    '#title' => t('Server Space notification cron'),
    '#description' => t('Check if you want to get email notification for server space for schedule cron. By defult it runs accrording to site predefined cron schedule.'),
   );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save'
  );
  return $form;
}


/**
 * @param $form, $form_state
 * @return $form
 */
function _server_disk_space_notification_settings_form_validate($form, $form_state) {
  $server_space_email_to = explode("\n", $form_state['values']['server_space_email_to']);

  foreach ($server_space_email_to as $key => $value) {
    if (!empty($value)) {
      if (FALSE == filter_var(trim($value), FILTER_VALIDATE_EMAIL)) {
        form_set_error('server_space_email_to', "Invalid email address -- " . trim($value));
      }
    }
  }

  $server_space_email_from_address = trim($form_state['values']['server_space_email_from_address']);
  if (empty($server_space_email_from_address)) {
    form_set_error('server_space_email_from_address', "Invalid from email address value -- " . $server_space_email_from_address);
  }

 }


/**
 * @param $form, $form_state
 * @return none
 */
function _server_disk_space_notification_settings_form_submit($form, $form_state) {
  $server_space_email_to = $form_state['values']['server_space_email_to'];
  $server_space_cron = $form_state['values']['server_space_cron'];
  $server_space_email_from_address = trim($form_state['values']['server_space_email_from_address']);
  $server_space_email_from_name = trim($form_state['values']['server_space_email_from_name']);
  $server_space = $form_state['values']['server_space'];

  variable_set('server_space_email_to', $server_space_email_to);
  variable_set('server_space_email_from_address', $server_space_email_from_address);
  variable_set('server_space_email_from_name', $server_space_email_from_name);
  variable_set('server_space_cron', $server_space_cron);
  variable_set('server_space', $server_space);

  if ($server_space != '') {
    _server_disk_space_notification_email_details($server_space_email_to);
  }
  drupal_set_message(t('Settings saved succesfully'));
}

/**
 * @param $server_space_email_to
 * @return none
 */
function _server_disk_space_notification_email_details($server_space_email_to = '') {

  $server_space_email_to = explode("\n", $server_space_email_to);
  foreach ($server_space_email_to as $key => $value) {
      $temp[] = trim($value);
  }
  $server_space_email_to = implode(',', $temp);
  $server_space_email_to = trim($server_space_email_to);
  $message = "<b><u>Server Disk Space</u></b>";
  $message .= "<pre>" . shell_exec('df -h') . "</pre>"; /* Body of your email here */
  $message .= "</br><b><u>Server RAM</u></b></br>";
  $message .= "<pre>" . shell_exec('free -m') . "</pre>"; /* Body of your email here */
  $todays_date = date("d-m-Y"); /* Today's date */
  $subject = 'Server Space Information for ' . $todays_date; /* Subject of your email */
  $params = array(
           'body' => $message,
           'subject' => $subject,
           'headers' => 'text/html; charset=UTF-8; format=flowed',
     );


    if (variable_get('server_space_email_from_name', '') != '') {
      $from_name = variable_get('server_space_email_from_name', '');
    }
    else {
      $from_name = variable_get('site_name', '');
    }

  $server_space_email_from_address = variable_get('server_space_email_from_address', '');
  $from = '"' . $from_name . '" <' . $server_space_email_from_address . '>';

  drupal_mail('server_disk_space_notification', 'send_notify', $server_space_email_to, language_default(), $params, $from);
  drupal_set_message(t('Email sent succesfully.'));
}

/**
 * @param $key, $message,  $params
 * @return none
 */
function server_disk_space_notification_mail($key, &$message, $params) {
  if ($key == 'send_notify') {
    $headers = array(
      'MIME-Version' => '1.0',
      'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
      'Content-Transfer-Encoding' => '8Bit',
      );

    $from = isset($message['params']['from']) ? trim($message['params']['from']) : '';
    if (!empty($from)) {
      $headers['from'] = $from;
      $headers['Reply-to'] = $from;
    }

    foreach ($headers as $key => $value) {
      $message['headers'][$key] = $value;
    }
    if (!empty($from)) {
      $message['from'] = $from;
    }

    $message['subject'] = $message['params']['subject'];
    $message['body'][] = $message['params']['body'];
  }
}


